pip install -r requirements.txt

usage: ncc_track.py [-h] [--source SOURCE] [--scale SCALE] [--debug]
                    [--update]

Tracking Demo

optional arguments:
  -h, --help       show this help message and exit
  --source SOURCE  source for frames [0 | file.mp4 | folder/]
  --scale SCALE    scale for frame resizing default 2(half frame)
  --debug          stop at every frame
  --update         update template at every timestep


To track without template update use 

python ncc_track.py 
