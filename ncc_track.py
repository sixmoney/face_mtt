import numpy as np
import cv2
import argparse
import os
import filetype

class image_source:
	def __init__(self, src):
		self.images = []
		self.num = 0
		self.media_type = ''
		self.vc = None
		try:
			camera_id = int(src)
			self.media_type = 'camera'
			self.vc = cv2.VideoCapture(camera_id)
		except:
			if os.path.isdir(src):
				self.media_type = 'image'
				files = os.listdir(src)
				for i in files:
					print (i)
					try:
						if 'image' in filetype.guess(src+'/'+i).mime:
							self.images.append(src+'/'+i)
					except:
						print(f'wrong media: {i}')
			else:
				try:
					if 'video' in filetype.guess(src).mime:
						self.media_type = 'video'
						self.vc = cv2.VideoCapture(src)
					elif 'image' in filetype.guess(src).mime:
						self.media_type = 'image'
						self.images.append(src)
				except:
					raise('Wrong Media Type ' + src)
		print(self.images)
	def __iter__(self):
		return self
	
	def __next__(self):
		if self.num < len(self.images) or self.media_type == 'camera' or self.media_type =='video':
			n = self.num
			self.num += 1	
			if self.media_type == 'image':
				print(self.images[n])
				return cv2.imread(self.images[n])	
			else:
				_, frame = self.vc.read()
				return frame
		else:
			raise StopIteration


drawing = False # true if mouse is pressed
mode = True # if True, draw rectangle. Press 'm' to toggle to curve
ix,iy = -1,-1

# mouse callback function
      

parser = argparse.ArgumentParser(description='Tracking Demo')
parser.add_argument('--source',  type=str, default='0', help='source for frames [0 | file.mp4 | folder/] ')
parser.add_argument('--scale',  type=int, default='2', help='scale for frame resizing default 2(half frame) ')
parser.add_argument('--debug', action='store_true', default=False, help='stop at every frame')
parser.add_argument('--update', action='store_true', default=False, help='update template at every timestep')

args = parser.parse_args()
scale = args.scale

source = image_source(args.source)
if args.debug:
    delay = 0
else:
    delay = 1
for i in range(10):
    img = next(source)
    img = cv2.resize(img,(0,0),fx=1.0/scale,fy=1.0/scale)
    
r = cv2.selectROI("frame", img)
template = img[int(r[1]):int(r[1]+r[3]), int(r[0]):int(r[0]+r[2])]

 

for id, frame in enumerate(source):
    img_bgr = cv2.resize(frame,(0,0),fx=1.0/scale,fy=1.0/scale)
    method  = cv2.TM_CCOEFF_NORMED
    res = cv2.matchTemplate(img_bgr,template,method)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
    if method in [cv2.TM_SQDIFF, cv2.TM_SQDIFF_NORMED]:
        top_left = min_loc
    else:
        top_left = max_loc
    bottom_right = (top_left[0] + r[2], top_left[1] + r[3])
    if args.update:
        r = [top_left[0],top_left[1], r[2] , r[3] ]
        template = img_bgr[int(r[1]):int(r[1]+r[3]), int(r[0]):int(r[0]+r[2])]
        template = template.copy()
    cv2.rectangle(img_bgr,top_left, bottom_right, 255, 2)
    cv2.imshow("frame",img_bgr)
    res = (res-min_val)/(max_val-min_val)
    res = (255*res)
    res = res.astype(np.uint8)
    res = cv2.applyColorMap(res, cv2.COLORMAP_JET)
    cv2.imshow("res", res)
    cv2.imshow("template", template)
    c = cv2.waitKey(delay)
    if c == 27: #if ESC is pressed exit
        break

