certifi==2020.12.5
filetype==1.0.7
numpy==1.20.3
opencv-python==4.5.2.52
dlib==19.18.0
