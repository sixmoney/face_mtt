#import skvideo.io
#import skvideo.datasets
#import skvideo.utils
import numpy as np
import cv2
import dlib
import time
import sys
import glob, os
import argparse

from numpy.lib.shape_base import _make_along_axis_idx
import filetype
from datetime import datetime

class track:
	max_id = 0
	def __init__(self, state, ttl=10):
		self.state = state
		self.ttl = ttl
		self.id = track.max_id
		self.color = tuple(np.random.randint(0,255,(3,)))
		track.max_id+=1
	

class image_source:
	def __init__(self, src):
		self.images = []
		self.num = 0
		self.media_type = ''
		self.vc = None
		try:
			camera_id = int(src)
			self.media_type = 'camera'
			self.vc = cv2.VideoCapture(camera_id)
		except:
			if os.path.isdir(src):
				self.media_type = 'image'
				files = os.listdir(src)
				for i in files:
					print (i)
					try:
						if 'image' in filetype.guess(src+'/'+i).mime:
							self.images.append(src+'/'+i)
					except:
						print(f'wrong media: {i}')
			else:
				try:
					if 'video' in filetype.guess(src).mime:
						self.media_type = 'video'
						self.vc = cv2.VideoCapture(src)
					elif 'image' in filetype.guess(src).mime:
						self.media_type = 'image'
						self.images.append(src)
				except:
					raise('Wrong Media Type ' + src)
		print(self.images)
	def __iter__(self):
		return self
	
	def __next__(self):
		if self.num < len(self.images) or self.media_type == 'camera' or self.media_type =='video':
			n = self.num
			self.num += 1	
			if self.media_type == 'image':
				print(self.images[n])
				return cv2.imread(self.images[n])	
			else:
				_, frame = self.vc.read()
				return frame
		else:
			raise StopIteration



def box_iou(pred_box, gt_box):
    '''
    Calculate iou for predict box and ground truth box
    Param
         pred_box: predict box coordinate
                   (xmin,ymin,xmax,ymax) format
         gt_box: ground truth box coordinate
                 (xmin,ymin,xmax,ymax) format
    Return
         iou value
    '''
    # get intersection box
    inter_box = [max(pred_box[0], gt_box[0]), max(pred_box[1], gt_box[1]), min(pred_box[2], gt_box[2]), min(pred_box[3], gt_box[3])]
    inter_w = max(0.0, inter_box[2] - inter_box[0] + 1)
    inter_h = max(0.0, inter_box[3] - inter_box[1] + 1)

    # compute overlap (IoU) = area of intersection / area of union
    pred_area = (pred_box[2] - pred_box[0] + 1) * (pred_box[3] - pred_box[1] + 1)
    gt_area = (gt_box[2] - gt_box[0] + 1) * (gt_box[3] - gt_box[1] + 1)
    inter_area = inter_w * inter_h
    union_area = pred_area + gt_area - inter_area
    return 0 if union_area == 0 else float(inter_area) / float(union_area) 
	



detector = dlib.get_frontal_face_detector()
#predictor_path = './shape_predictor_68_face_landmarks.dat'
#predictor = dlib.shape_predictor(predictor_path)


parser = argparse.ArgumentParser(description='Tracking Demo')
parser.add_argument('--source',  type=str, default='0', help='source for frames [0 | file.mp4 | folder/] ')
parser.add_argument('--scale',  type=int, default='2', help='source for frames [0 | file.mp4 | folder/] ')
parser.add_argument('--debug', action='store_true', default=False)
parser.add_argument('--ttl', type=int, default=30)

args = parser.parse_args()


source = image_source(args.source)

last_crop=np.zeros((128,128,3),np.uint8)

scale = args.scale
offset = 80

if source.media_type == 'image':
	delay=0
else:
	delay=1
if args.debug:
	delay=0
tracks = []
font = cv2.FONT_HERSHEY_SIMPLEX 
		
for id_img, frame in enumerate(source):
	t1 = time.time()	

	
	img_bgr = cv2.resize(frame,(0,0),fx=1.0/scale,fy=1.0/scale)
	
	img = cv2.cvtColor(img_bgr,cv2.COLOR_BGR2GRAY)

	dets = detector(img, 1)

	M = np.zeros((len(dets), len(tracks)))
	A = np.zeros((len(dets), len(tracks)))
	for i, d in enumerate(dets):
		for j, t in enumerate(tracks):
			d_b = (d.left(), d.top(), d.right(), d.bottom())
			t_b = (t.state.left(), t.state.top(), t.state.right(), t.state.bottom())
			M[i, j] = box_iou( d_b, t_b)
	print(f'M:{M}')
	print(f'A:{A}')
	print('='*80)
	

	# associate
	M=M+np.random.rand(M.shape[0], M.shape[1])*1e-4 #add noise to avoid identical max...ugly
	if M.size>0:
		associating = True
		ndets = len(dets)-np.sum(A)
		ntracks = len(tracks)-np.sum(A)
		while associating and ntracks>0 and ndets>0:
		
			d_id, t_id = np.where(M == np.max(M))
			if (A[d_id, t_id] == 0).any():
				print(M[d_id, t_id])
				if M[d_id, t_id].squeeze() > .25:
					A[d_id, t_id] = 1
					M[d_id, :] = 0
					M[:, t_id] = 0
					tracks[t_id[0]].state = dets[d_id[0]] #update
					tracks[t_id[0]].ttl=args.ttl
				else:
					associating = False
			ndets = len(dets)-np.sum(A)
			ntracks = len(tracks)-np.sum(A)
		
	# for each unassociated track reduce ttl
	for j, t in enumerate(tracks):
		if np.all(A[:, j]==0): #row i is all zeros
			t.ttl -= 1
	
	# for each unassociated det start a new track
	for i, d in enumerate(dets):
		if np.all(A[i,:]==0): #row i is all zeros
			tracks.append(track(d, ttl=args.ttl))
			
	for t in tracks.copy():
		if t.ttl == 0:
			tracks.remove(t)
	
	 
	#for d in dets:
	#	cv2.rectangle(img_bgr,(d.left(),d.top()),(d.right(),d.bottom()), (255,0,0))
	for t in tracks:
		col = (int(t.color[0]), int(t.color[1]), int(t.color[2]))
		cv2.rectangle(img_bgr,(t.state.left(),t.state.top()), (t.state.right(),t.state.bottom()), col)
		fontScale = .75

		cv2.putText(img_bgr, f'{t.id}:{t.ttl}', (t.state.left()-1,t.state.top()-1), font, fontScale, col, 1, cv2.LINE_AA) 

			

		fps = int(1.0/(time.time()-t1))
		  
		# org 
		org = (0, img_bgr.shape[0]-20) 
		# fontScale 
		fontScale = .5
		   
		# Blue color in BGR 
		color = (255, 124, 0) 
		  
		# Line thickness of 2 px 
		thickness = 1
		cv2.putText(img_bgr, str(fps), org, font, fontScale, color, thickness, cv2.LINE_AA) 
		
	else:
		print("miss!")
		
	
	c=0
	cv2.namedWindow("Frame", cv2.WINDOW_NORMAL)
	cv2.imshow("Frame", img_bgr)
	c = cv2.waitKey(delay)
	if c == 27: #if ESC is pressed exit
		break
	if c > 0:
		c = chr(c)

	

